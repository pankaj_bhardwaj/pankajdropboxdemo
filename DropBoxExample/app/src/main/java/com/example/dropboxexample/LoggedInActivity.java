package com.example.dropboxexample;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

public class LoggedInActivity extends AppCompatActivity {

    private String accessToken;
    private TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);

        textView = findViewById(R.id.data);

        SharedPreferences preferences = getSharedPreferences("com.example.dropboxeample", Context.MODE_PRIVATE);
        accessToken = preferences.getString("access-token", "no");
        if (accessToken != null && !accessToken.equalsIgnoreCase("no")) {
            new UserAccountTask(DropBoxClient.getClient(accessToken), new UserAccountTask.TaskDelegate() {
                @Override
                public void onAccountReceived(FullAccount account) {
                    updateUI(account);
                }

                @Override
                public void onError(Exception error) {
                    Log.d("SOME_EXCEPTION", "THIS");
                }
            }).execute();
        }
        final Button button = findViewById(R.id.getFile);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoggedInActivity.this, FilesActivity.class);
                startActivity(intent);
            }
        });

    }

    private void updateUI(FullAccount account) {
        ImageView profile = findViewById(R.id.imageView);
        TextView email = (TextView) findViewById(R.id.data);
        email.setText(account.getEmail());

        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(this))
                .addRequestHandler(new FileThumbnailRequestHandler(DropBoxClient.getClient(accessToken)))
                .build();
        picasso.load(account.getProfilePhotoUrl()).resize(200, 200).into(profile);

    }



}

package com.example.dropboxexample;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dropbox.core.android.Auth;


public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences preferences = getSharedPreferences("com.example.dropboxeample", Context.MODE_PRIVATE);
        String accessToken = preferences.getString("access-token", "no");
        if (accessToken != null && !accessToken.equalsIgnoreCase("no")) {

            Intent intent = new Intent(MainActivity.this, LoggedInActivity.class);
            startActivity(intent);

        } else {
            Button signIn = findViewById(R.id.signIn);

            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Auth.startOAuth2Authentication(getApplicationContext(), getString(R.string.key));
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAccessToken();
    }

    private void getAccessToken() {
        String accessToken = Auth.getOAuth2Token();
        if (accessToken != null) {
            SharedPreferences preferences = getSharedPreferences("com.example.dropboxeample", Context.MODE_PRIVATE);
            preferences.edit().putString("access-token", accessToken).apply();
            Intent intent = new Intent(MainActivity.this, LoggedInActivity.class);
            startActivity(intent);
        }
    }

}